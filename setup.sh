echo "First we need to copy ssh keys across"
ansible-playbook -i good-hosts.yml CopySshKey.yml

echo "Now the setup begins"
ansible-playbook -i good-hosts.yml pi-setup.yml
